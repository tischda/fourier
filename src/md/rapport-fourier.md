---
grade: |
    Département Informatique

    5\ieme{} année

    2013 - 2014

document:
    type: Note de synthèse
    title: La Transformée de Fourier
    short: La Transformée de Fourier
    abstract: |
        Fourier transform.

    keywords: fourier, signal processing, spectral analysis, sound
    frenchabstract: |

        Note de synthèse sur la Transformée de Fourier.

    frenchkeywords: fourier, traitement du signal, analyse spectrale

supervisors:
    category: Encadrant
    list:
        - name: Gilles VENTURINI
          mail: gilles.venturini@univ-tours.fr
    details: "École polytechnique de l'université de Tours"

authors:
    category: Étudiant
    list:
        - name: Daniel TISCHER
          mail: daniel.tischer@etu.univ-tours.fr
    details: DI5 2013 - 2014

preamble:
    include.tex
---

Introduction
============

Joseph Fourier
--------------

Jean Baptiste Joseph Fourier est un mathématicien et physicien français né le 21
mars 1768 à Auxerre et mort le 16 mai 1830 à Paris. Il s'intéresse à l'étude de
la chaleur et découvre une équation décrivant sa propagation dans les corps
solides. C'est ainsi qu'il met au point la méthode d'analyse qui porte son nom.

![Joseph Fourier](images/fourier-bio.jpg)

Les séries de Fourier
---------------------

On rencontre les séries de Fourier dans l'étude des courants électriques, des
ondes cérébrales, dans la synthèse sonore, le traitement d'images, etc. Elles
sont un outil fondamental dans l'étude des fonctions périodiques.

### En théorie

La théorie des séries de Fourier établit une correspondance entre la fonction
périodique et les coefficients éponymes. L'idée est d'obtenir une fonction
admettant T pour période comme une somme de fonctions sinusoïdales :
$$f(x)=\sum_{n=-\infty}^{+\infty}c_n (f)e^{i 2\pi\frac{n}{T} x}$$ avec les
coefficients définis par la formule : $$c_n(f) = \frac1T\int_{-T/2}^{T/2} f(t)
e^{-i \frac{2n\pi}{T}t}\,\mathrm{d}t\, $$

### En pratique

A titre d'exemple, nous allons approximer un signal carré par une série de
Fourier. Ce signal alterne entre deux valeurs (-1 et 1) de façon périodique.
Nous pouvons  approximer cette fonction avec une somme des courbes de sinus.

Pour ce faire, nous appliquons la série de Fourier suivante sur l'intervalle
[0, 1] :
$$f(x)=\frac{4}{\pi} \sum_{j=impair}^{+\infty} \frac{1}{j \cdot sin(j 2 \pi x)}$$

Dans la somme, on n'utilise que des valeurs impaires. En effet, contrairement
au signal en dents de scie, le signal carré ne comprend que les harmoniques
entières impaires.

Voici le code en R:

~~~
pdf(width = 7, height = 3)
par(pin=c(1.8,1.3), mfrow=c(1,2), cex.lab=0.75, cex.axis=0.5, mar=c(5.1, 4.1, 2.1, 2.1))

plot.series <- function(M, x) {
  # signal carré
  f <- sign(1/2-x)
  plot(x, f, type="l", col="blue", xlab=paste("M = ",M," coefficients"),
    ylab="", xlim=c(0, 1), ylim=c(-1.5, 1.5))

  # série de Fourier
  sum=0
  for (j in seq(1,M,2)) {
      sum = sum + 4/pi*sin(j*2*pi*x)/j
  }
  lines(x, sum, col="red")
}

plot.series( 5, x=seq(0,1,1/100))
plot.series(15, x=seq(0,1,1/100))
~~~

Plus on applique de coefficients, et plus la courbe (en rouge) va ressembler au
signal carré (en bleu) :

![Approximation d'un signal carré](images/plot-square-approx.pdf)

Transformation de Fourier discrète (DFT)
----------------------------------------

La transformation de Fourier est une extension, pour les fonctions non
périodiques, du développement en série des fonctions périodiques de Fourier.
La transformée de Fourier s'exprime comme « somme infinie » des fonctions
trigonométriques de toutes fréquences (en bleu).

Le spectre d'un signal discret obtenu par échantillonnage à la période T
est périodique.

![Domaine fréquentiel (M = 15)](images/spectre2.png)

On appellera donc la fonction F(x) le _signal_ et sa transformée de Fourier son
_spectre_.

La transformée de Fourier rapide (FFT)
--------------------------------------
La transformée de Fourier rapide (FFT ou Fast Fourier Transform) est un
algorithme de calcul de la transformée de Fourier discrète (DFT).

Sans optimisation des calculs, une transformation de Fourier d'ordre N exige le
calcul de N^2^ multiplications complexes. L'algorithme FFT (Fast Fourier
Transform) élimine les redondances et détermine le résultat avec seulement
N log~2~(N) opérations. La condition nécessaire pour l'utilisation de cet
algorithme est que N soit choisi comme une puissance de 2.

Cet algorithme est couramment utilisé en traitement numérique du signal pour
transformer des données discrètes du domaine temporel dans le domaine
fréquentiel, en particulier dans les analyseurs de spectre.

### Exemple d'utilisation

Voici un exemple dans R :

~~~
plot.fourier <- function(fourier.series, f.0, ts) {
  w <- 2*pi*f.0
  trajectory <- sapply(ts, function(t) fourier.series(t,w))
  plot(ts, trajectory, type="l", xlab="Time", ylab="f(t)"); abline(h=0,lty=3)
  X.k <- fft(trajectory)
  plot.frequency.spectrum(X.k, xlimits=c(0,20))
}
~~~

Pour tester différents paramètres, on définit la fonction __plot.fourier__ qui
prend trois arguments :

* __fourier.series__ : une fonction trigonométrique

* __f.0__ : la fréquence fondamentale de la courbe, ou longueur d'onde. Les
  fréquences doivent être des multiples de la fréquence fondamentale. $f_0$ est
  appelée la première harmonique, la seconde harmonique est $2 \times f_0 $, la
  troisième $3 \times f_0 $, etc.

* __ts__ : le vecteur des valeurs échantillonnées (sampling time points)

On applique alors la fonction `fourier.series()` sur tous les points de `ts`,
puis on stocke les résultats dans `trajectory` pour tracer la trajectoire ainsi
définie. Enfin, on exécute la fonction `fft()` sur cette trajectoire.

### Densité spectrale
Le périodogramme permet une estimation simple de la densité spectrale de
puissance en prenant le carré de la transformée de Fourier. Il est implémenté
par la fonction suivante :

~~~
plot.frequency.spectrum <- function(X.k, xlimits=c(0,length(X.k))) {
  plot.data  <- cbind(0:(length(X.k)-1), Mod(X.k))
  plot.data[2:length(X.k),2] <- 2*plot.data[2:length(X.k),2]
  plot(plot.data, t="h", lwd=2, main="",
       xlab="Frequency", ylab="Strength",
       xlim=xlimits, ylim=c(0,max(Mod(plot.data[,2]))))
}
~~~

Grâce à ces deux fonctions, nous pouvons maintenant faire quelques tests
d'analyse spectrale :

~~~
plot.fourier(function(t,w) {sin(w*t)}, 1, ts=seq(0,1,1/100))
plot.fourier(function(t,w) {sin(w*t)}, 4, ts=seq(0,1,1/100))
~~~

![](images/plot-sine-freq1.pdf)
![Courbe de sinus avec une fréquence de 1 et 4](images/plot-sine-freq4.pdf)


Application pratique
====================

Problème de la glycémie
-----------------------

Nous avons choisi d'utiliser la transformée de Fourier pour détecter les phases
de glycémie d'un patient afin de prédire son état de santé.

![Concentration de glucose dans le sang](images/glucose.pdf)


Cependant, nous n'avons pas pu obtenir un échantillon suffisamment grand pour
reconstruire un signal périodique sur 24h. Le seul échantillon disponible
comportait des trous et aurait demandé une segmentation des données.

Il nous manquait également l'interprétation des phases qui aurait permis ce
découpage et l'apprentissage nécessaire au pronostic.

Par ailleurs, j'ai découvert qu'une analyse similaire avait déjà été effectuée
avec succès :

> Use of Fourier Models for Analysis and Interpretation of Continuous Glucose Monitoring Glucose Profiles
> J Diabetes Sci Technol. Sep 2007; 1(5): 630–638.
> Published online Sep 2007.
>
> [http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2769655/](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2769655/)

> Optimum Subcutaneous Glucose Sampling and Fourier Analysis of Continuous Glucose Monitors
> J Diabetes Sci Technol. May 2008; 2(3): 495–500.
> Published online May 2008.
>
> [http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2769727/](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2769727/)


Classification de signal
------------------------

Je vais décrire ici la méthode que nous aurions utilisée sur les mesures de
glucose. Il s'agit d'appliquer la transformation de Fourier à un signal et de
comparer ses coefficients à des éléments connus.

Pour cela, nous allons réutiliser les deux courbes présentées en introduction
(la courbe de sinus et l'approximation du signal carré). Nous appliquerons
ensuite la transformation de Fourier sur un nouvel échantillon inconnu et
comparerons ses coefficients à ceux des signaux connus par la méthode des KPPV.

### Analyse des données

Le résultat de la transformée de Fourier rapide (FFT) pour le signal carré (M =
5) est le suivant :

~~~
  [1] -1.915135e-15+0.000000e+00i  1.995889e+00-6.414574e+01i
  [3] -1.550444e-02+2.489069e-01i  1.960701e+00-2.095070e+01i
  [5] -6.948918e-02+5.556222e-01i  1.869018e+00-1.192048e+01i
  [7] -2.307415e-01+1.221975e+00i -1.726478e-01+7.803591e-01i
  [9] -1.531539e-01+6.027176e-01i -1.433865e-01+4.987468e-01i
 [11] -1.375743e-01+4.279347e-01i -1.337605e-01+3.755609e-01i
~~~

Chaque valeur correspond à une composante de fréquence différente. La
FFT nous donne l'amplitude et la phase. L'amplitude
est la magnitude ($r=|z|=\sqrt{x^2+y^2}$) du nombre complexe alors que la phase
est représentée par l'angle $\varphi$ :

\begin{figure}[!ht]
  \centering
  \includegraphics[width=4cm]{images/complexe.png}
  \caption{Nombre complexe}
\end{figure}


K-Plus Proches Voisins (KPPV)
-----------------------------

### Principe

Dans un espace de dimension $d$, on recherche à déterminer quels sont les k
points de l'ensemble A les plus proches d'un point donné x :

\begin{figure}[!ht]
  \centering
  \includegraphics[width=8cm]{images/kppv.png}
  \caption{Voisinage de taille 3 autour d'une coordonnée donnée (d = 1, k = 3).}
\end{figure}

La distance euclidienne entre deux exemples $X = (x_1, \ldots, x_d)$ et
$Y = (y_1, \ldots, y_d)$ se calcule avec :

$$dist(X, Y) = \sqrt{\sum_{i=1}^{d}(x_i-y_i)^2}$$

Dans notre cas, le vecteur de caractéristiques d'un exemple de sinusoïde sera
formé à partir des composantes (partie réelle et imaginaire) de Fourier :
$Re_1,Im_1,Re_2,Im_2,Re_3,Im_3,\ldots,Re_d,Im_d$

### Démonstration

La méthode des k-plus proches voisins est une méthode d’apprentissage supervisé.
Cela signifie qu'il faut préparer un certain nombre de signaux de chaque classe
pour permettre au système d'apprendre les exemples qui appartiennent ou non à
l'une ou l'autre des classes.

Dans le code qui suit, on génère une population de 90 individus. La première
moitié contient les coefficients des courbes de sinus pour les fréquences de 1 à
45 ; dans la seconde moitié, nous faisons varier le nombre M de coefficients
de la série de Fourier pour le signal carré :

~~~
s = 100       # number of sampling points
d = s*2+1     # cols : dimension of feature vector (Re+Im) + 1 for class
N = 90        # rows : total number of waves (50% sine, 50% square)
n = N/2

waves <- matrix(data=NA, nrow=N, ncol=d)

w <- 2*pi
ts=seq(0,1,1/(s-1))
fourier.series <- function(t,w) {sin(w*t)}

# generate sine data set
for(f in 1:n) {
    trajectory <- sapply(ts, function(t) fourier.series(t, w*f))
    X.k <- fft(trajectory)
    x = as.vector(rbind(Re(X.k), Im(X.k)))
    waves[f,] <- c("sine", x)
}

# generate square data set
for (M in 1:n) {
    sum = 0
    for (j in seq(1,M+1,2)) {
        sum = sum + 4/pi*sin(j*2*pi*ts)/j
    }
    X.k <- fft(sum)
    x = as.vector(rbind(Re(X.k), Im(X.k)))
    waves[n+M,] <- c("square", x)
}

# convert matrix to data frame
waves <- as.data.frame(waves)
waves[1] <- lapply(waves[1], as.factor)
~~~

Nous obtenons ainsi les données suivantes, dont voici un extrait :

~~~
> waves[40:50,1:6]
       V1                    V2 V3                V4                  V5                  V6
40   sine  5.32907051820075e-14  0 0.000307120018621987  -0.0097727174516562  0.00123126386873795
41   sine  5.55111512312578e-14  0 0.000273065586001486 -0.00868908783499034  0.00109467158221399
42   sine  1.93400850889702e-13  0 0.000239605568400858 -0.00762437281067418 0.000960486111076797
43   sine  -3.1641356201817e-14  0 0.000206658556302086 -0.00657598188452096 0.000828377176265382
44   sine -7.43849426498855e-14  0 0.000174147671510694 -0.00554146876267519 0.000698033120620334
45   sine -4.59632332194815e-14  0  0.00014199980835039 -0.00451850716852686 0.000569157773954387
46 square -1.72084568816899e-15  0      1.9892818761214     -63.299975675674  -0.0542209522134583
47 square -7.49400541621981e-16  0     1.99420467062859    -63.4566215368273  -0.0231315826139281
48 square -7.49400541621981e-16  0     1.99420467062859    -63.4566215368273  -0.0231315826139281
49 square -3.44169137633799e-15  0     1.99583944830608    -63.5086410059945  -0.0156803231204043
50 square -3.44169137633799e-15  0     1.99583944830608    -63.5086410059945  -0.0156803231204043
~~~

Voyons maintenant si ces données permettent de classifier une courbe en
définissant l'ensemble d'apprentissage `train` à partir d'un échantillon
aléatoire (1/9 de la population) et en appliquant la méthode des KPPV aux
individus restants (ensemble de test) :

~~~
library(class)
classes <- waves[,1]
train <- sample(1:N, N/9)
result1 <- knn(waves[train,2:d], waves[-train,2:d], classes[train], k=1)
table1 <- table(old=classes[-train], new=result1)
~~~

On obtient la matrice de confusion suivante :

~~~
> table1
        new
old      sine square
  sine     36      1
  square    0     43
> chisq.test(table1)$statistic
X-squared
 72.18838
~~~

On voit ici que les individus ont été parfaitement classés, à une exception près
(un signal sinusoïdal classé comme signal carré). Ceci est normal, puisque la
série de Fourier utilisée pour le signal carré avec un seul coefficient (M=1)
est en fait une sinusoïde (il faudra donc penser à éliminer cet individu de
l'ensemble d'apprentissage).

### Application sur un exemple concret

Pour tester la classification d'un signal inconnu, nous allons installer le
package R _seewave_ [[http://rug.mnhn.fr/seewave/](http://rug.mnhn.fr/seewave/)]
spécialisé dans l'analyse et la synthèse de son. Il nous intéresse surtout pour
sa synthèse du chant de la cigale `orni`[^1]. Cette espèce se trouve en Europe
centrale, au Proche-Orient et en Amérique du nord. Seuls les mâles produisent le
chant qui a pour fonction l'attraction sexuelle des femelles.

&nbsp;

\begin{figure}[!ht]
    \centering
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=5.7cm]{images/cicada.jpeg}
      \caption{\textit{Cicada orni}, vue de dos}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=7cm]{images/orniwave-crop.pdf}
      \caption{Chant de la cigale}
    \end{subfigure}
\end{figure}

[^1]: [http://en.wikipedia.org/wiki/Cicada_orni](http://en.wikipedia.org/wiki/Cicada_orni)

Les commandes suivantes installent le package, chargent le data set _orni_ et
affichent le signal (b).

~~~
install.packages("seewave")
library("seewave")
data(orni)
envorni <- env(orni, f=22050, plot=FALSE)   # Amplitude envelope
ts <- seq_along(envorni)
plot(ts, envorni, type="l", xlab="", ylab="")
~~~

Dans notre ensemble d'apprentissage, nous n'avons que 100 coefficients, il nous
faut donc échantillonner le signal d'origine (c'est à dire extraire 100 points
sur les 15842 du signal) :

~~~
ts <- seq(1, dim(envorni)[1], dim(envorni)[1]/100)
plot(ts, envorni[ts], type="l", xlab="", ylab="")
~~~

\begin{figure}[!ht]
  \centering
  \includegraphics[width=7.5cm]{images/orniwavesamp-crop.pdf}
  \caption{Chant de la cigale échantillonné}
\end{figure}

Appliquons maintenant la transformée de Fourier :

~~~
trajectory <- t(as.data.frame(envorni[ts]))
X.k <- fft(trajectory)
~~~

Enfin, en exécutant la classification par les k-plus proches voisins, nous
trouvons un signal carré :

~~~
> test = t(as.data.frame(as.vector(rbind(Re(X.k), Im(X.k)))))
> knn(waves[train,2:d], test, classes[train], k=1)
[1] square
Levels: sine square
~~~


Conclusion
----------

Nous avons d'abord étudié les bases théoriques de la transformée de Fourier
comme préalable à son utilisation dans le domaine du traitement du signal.

Dans une seconde partie, nous avons vu son application concrète avec le logiciel
R sur la classification du signal. En dehors de la reconnaissance sonore, cette
méthode offre des possibilités intéressantes dans le domaine médical, comme nous
l'avons vu pour l'analyse de la concentration de glucose dans le sang.


\nocite{desgraupes_livre_2013}
\nocite{tisserand_analyse_2004}
\nocite{beoesch_comprendre_1999}
\nocite{FFTW05}
\nocite{bennet-clark_model_1992}
\nocite{bennet-clark_role_????}
\nocite{mauguit_chant_2013}
\nocite{butz_fourier_2010}
\nocite{riffle_understanding_????}
\nocite{stackoverflow_fast_????}
\nocite{lutus_exploring_2009}
\nocite{harb_classification_2003}

