::-----------------------------------------------------------------------------
:: This script executes the R command script
::-----------------------------------------------------------------------------
@echo off
setlocal
set BUILD_DIR=build
set RFILE=glucose.R
set RDATA=scbrdata.csv

if "%1"=="clean" goto clean

if not exist %BUILD_DIR% md %BUILD_DIR%

::-----------------------------------------------------------------------------
:build

pushd build
copy ..\%RFILE%
copy ..\%RDATA%
r CMD BATCH -q --vanilla %RFILE%
del %RFILE%
del %RDATA%
popd

goto end
::-----------------------------------------------------------------------------
:clean

echo . Cleaning %BUILD_DIR% directory
rd /s/q %BUILD_DIR%

:end
endlocal
