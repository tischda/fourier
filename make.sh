#!/bin/bash
# -----------------------------------------------------------------------------
# This script transforms Markdown and LaTeX sources into PDF
# -----------------------------------------------------------------------------
#  Usage:
#    make                  compiles md and tex sources in ./src
#    make clean            removes build files
# -----------------------------------------------------------------------------

BUILD_DIR=build

export PANDOC_OPTS="-V classoption=UTF8"
export PANDOC_OPTS="${PANDOC_OPTS} -V classoption=twoside"
#export PANDOC_OPTS="${PANDOC_OPTS} -V classoption=fourside"
#export PANDOC_OPTS="${PANDOC_OPTS} -V classoption=landscape"
#export PANDOC_OPTS="${PANDOC_OPTS} -V classoption=draft"
#export PANDOC_OPTS="${PANDOC_OPTS} -V classoption=final"
#export PANDOC_OPTS="${PANDOC_OPTS} -V classoption=CDS"

# -----------------------------------------------------------------------------
# Clean

if [ "$1" = "clean" ] ; then
	echo . Cleaning ${BUILD_DIR} directory
	rm -rf ${BUILD_DIR}
	exit
fi

[ -d ${BUILD_DIR} ] || mkdir ${BUILD_DIR}

# -----------------------------------------------------------------------------
# Main

if [ "$1" = "" ] ; then
    echo . Processing Markdown
    INPUT="rapport-fourier.md bibliographie.md"
    ./scripts/compile-md.sh ${INPUT} > /dev/null 2>&1
fi
